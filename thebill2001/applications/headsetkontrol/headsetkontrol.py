import info

class subinfo(info.infoclass):
    def setTargets(self):
        self.displayName = "HeadsetKontrol"
        self.description = "Interface for HeadsetControl written with Kirigami and KDE Framework"
        self.webpage = "https://gitlab.com/TheBill2001/HeadsetKontrol"
        self.svnTargets["master"] = "https://gitlab.com/TheBill2001/HeadsetKontrol.git"

    def setDependencies(self):
        self.buildDependencies["kde/frameworks/extra-cmake-modules"] = None

        # self.runtimeDependencies["libs/qt5/qtbase"] = None
        self.runtimeDependencies["kde/frameworks/tier1/kconfig"] = None
        self.runtimeDependencies["kde/frameworks/tier1/kdbusaddons"] = None
        self.runtimeDependencies["kde/frameworks/tier1/kcoreaddons"] = None
        self.runtimeDependencies["kde/frameworks/tier1/kirigami"] = None
        self.runtimeDependencies["kde/frameworks/tier1/ki18n"] = None
        self.runtimeDependencies["kde/frameworks/tier3/knotifications"] = None

from Package.CMakePackageBase import *

class Package(CMakePackageBase):
    def __init__( self ):
        CMakePackageBase.__init__( self )
        # CMakePackageBase.buildTests = False

    def createPackage(self):
        self.defines["appname"] = "HeadsetKontrol"
        return super().createPackage()
